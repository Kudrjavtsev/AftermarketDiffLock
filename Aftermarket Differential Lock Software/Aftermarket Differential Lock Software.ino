const int FRONT_LEFT_OUTPUT = 1;
const int FRONT_RIGHT_OUTPUT = 3;
const int REAR_LEFT_OUTPUT = 2;
const int REAR_RIGHT_OUTPUT = 4;
const int FRONT_LEFT_INPUT = A0;
const int FRONT_RIGHT_INPUT = A1;
const int REAR_LEFT_INPUT = A2;
const int REAR_RIGHT_INPUT = A3;
const int WHEELS[] = {FRONT_LEFT_OUTPUT, FRONT_RIGHT_OUTPUT, REAR_LEFT_OUTPUT, REAR_RIGHT_OUTPUT};
const int WHEEL_SENSORS[] = {FRONT_LEFT_INPUT, FRONT_RIGHT_INPUT, REAR_LEFT_INPUT, REAR_RIGHT_INPUT};
const int SCAN_INTERVAL = 100;
const int HOLD_DURATION = 500;
const int POST_HOLD_DURATION = 500;
const float TRIGGER_MIN = 0.50;
const float TRIGGER_MAX = 1.00;
const float BASE_VOLTAGE = 0.0;
const int PUMP = 6;
const double TRIGGER_DIFFERENCE = 0.3;

void setupInput() {
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
}

void setup() {
  setupInput();
  for (int i = 0; i<(sizeof(WHEELS)-1/sizeof(int)); i++) {
    pinMode(WHEELS[i], OUTPUT);
  }
  pinMode(PUMP, OUTPUT);
}

void loop() {
  scanForInput();
}

double convertToVoltage(int value) {
  return value * (5.0 / 1023.0);
}

void closeValve(int pin) {
  String message = "Closing valve: ";
  message += pin;
  Serial.println(message); 
  digitalWrite(pin, HIGH);
  digitalWrite(PUMP, HIGH);
  delay(HOLD_DURATION);
  digitalWrite(pin, LOW);
  digitalWrite(PUMP, LOW);
  String releaseMessage = "Releasing valve: ";
  releaseMessage += pin;
  Serial.println(releaseMessage);
  delay(POST_HOLD_DURATION);
}

boolean shouldClose(double voltage, double oppositeVoltage) {
  return voltage == BASE_VOLTAGE && oppositeVoltage != BASE_VOLTAGE;
}

void scanForInput(){
  double frontLeft = convertToVoltage(analogRead(WHEEL_SENSORS[0]));
  double frontRight = convertToVoltage(analogRead(WHEEL_SENSORS[1]));
  double rearLeft = convertToVoltage(analogRead(WHEEL_SENSORS[2]));
  double rearRight = convertToVoltage(analogRead(WHEEL_SENSORS[3]));
  String values = "FL: ";
  values += frontLeft;
  values += ", FR: ";
  values += frontRight;
  Serial.println(values);
  if(abs(frontLeft - frontRight) > TRIGGER_DIFFERENCE) {
    if(shouldClose(frontLeft, frontRight)) {
      closeValve(FRONT_LEFT_OUTPUT);
    } else if (shouldClose(frontRight, frontLeft)) {
      closeValve(FRONT_RIGHT_OUTPUT);
    }
  }
}

